// import logo from './logo.svg';
// import './App.css';
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "../src/component/Home";
import SideBar from "./component/SideBar";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <main>
          <Switch>
            <div>
              {" "}
              <SideBar />
              <Route path="/" component={Home} exact />
            </div>
          </Switch>
        </main>
      </BrowserRouter>
    </div>
  );
}

export default App;
